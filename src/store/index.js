import Vue from "vue";
import Vuex from "vuex";
import * as R from "ramda";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    toastId: 1,
    toastList: [],
    toastTime: 5000,
  },
  getters: {
    toastId: (state) => state.toastId,
    toastList: (state) => state.toastList,
  },
  mutations: {
    increaseToastId(state) {
      state.toastId++;
    },
    setToastList(state, value) {
      state.toastList = value;
    },
    addToast(state, value) {
      state.toastList.push(value);
    },
    removeToast(state, index) {
      state.toastList.splice(index, 1);
    },
    setToastTimeId(state, { index, newTimeId }) {
      state.toastList[index].timeId = newTimeId;
    },
    setToastTime(state, value) {
      state.toastTime = value;
    },
  },
  actions: {
    addToast({ commit, state, dispatch }) {
      const timer = setTimeout(
        (id) => dispatch("remove", id),
        state.toastTime,
        state.toastId
      );
      const randomColor =
        "#" + Math.floor(Math.random() * 16777215).toString(16);
      commit("addToast", {
        id: state.toastId,
        timeId: timer,
        color: randomColor,
      });
      commit("increaseToastId");
    },
    remove({ state, commit }, toastId) {
      const removeToastIndex = state.toastList.findIndex(
        ({ id }) => id === toastId
      );
      commit("removeToast", removeToastIndex);
    },
    pause({ state }, id) {
      const pauseToast = R.find(R.propEq("id", id))(state.toastList);
      if (R.isNil(pauseToast)) {
        return;
      }
      clearTimeout(pauseToast.timeId);
    },
    restart({ state, commit, dispatch }, toastId) {
      const startToast = R.find(R.propEq("id", toastId))(state.toastList);
      if (R.isNil(startToast)) {
        return;
      }
      const newTimeId = setTimeout(
        (id) => dispatch("remove", id),
        state.toastTime,
        startToast.id
      );
      const index = state.toastList.findIndex(({ id }) => id === toastId);
      commit("setToastTimeId", { index, newTimeId });
    },
  },
  modules: {},
});
